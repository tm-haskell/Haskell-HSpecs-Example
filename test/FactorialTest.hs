module FactorialTest where

import Test.Hspec
import HW01 
import TestTemplate

factTests = hspec $ do 
    describe "Fact Tests" $ do
        subTest "factorial base is 1"
            (factorial 0 `shouldBe` (1 :: Integer))

        subTest "factorial 3 is 5"
            (factorial 3 `shouldBe` (6 :: Integer))
        
        subTest "factorial 6 is 720"
            (factorial 6 `shouldBe` (720 :: Integer))


    describe "factorialsM" $ do
        subTest "factorialsM base test"
            (take 1 factorialsM `shouldBe` [1])

        subTest "factorialsM general test"
            (take 5 factorialsM `shouldBe` [1, 2, 6, 24, 120])

    


