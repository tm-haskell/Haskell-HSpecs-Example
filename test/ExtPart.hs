module ExtPart where 

import Test.Hspec
import Control.Exception
import HW01 
import TestTemplate

extPartTests = hspec $ do 
    describe "ExtPart" $ do

        subTest "throws if negative length"
            (evaluate (expPart 2 (-1)) `shouldThrow` anyErrorCall)

        subTest "throws if negative base"
            (evaluate (expPart (-1) 1) `shouldThrow` anyErrorCall)

        subTest "factorial base elements"
            ((expPart 2 2) `shouldBe` sum [1, 2.0 / 2.0])

        subTest "general case"
            ((expPart 2 4) `shouldBe` sum [1, 2.0 / 2.0, 4.0 / 6.0, 8.0 / 24.0])
            