module ListSum where

import Test.Hspec
import HW01 
import TestTemplate

allListsumTests = do 
    bothEmpty
    eitherEmpty
    genCase
    unevenLength
    
bothEmpty = test "listSum returns [] for [],[]" 
    (listSum [] [] `shouldBe` ([]))

eitherEmpty = test "listSum returns [] in either arg is []" 
    (listSum [1, 2] [] `shouldBe` ([]))

genCase = test "listSum general case" 
    (listSum [1, 2] [2, 3] `shouldBe` ([3, 5]))

unevenLength = test "listSum returns list of smaller length when lengths uneven" 
    (listSum [1, 2] [2] `shouldBe` ([3]))

-- fact_general_case = fact_test "6! is 720" 
--     (factorial 6 `shouldBe` (720 :: Integer))


