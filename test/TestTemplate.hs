module TestTemplate where

import Test.Hspec

test :: [Char] -> IO () -> IO()
test = testDescr ""

-- subTest :: [Char] -> Expectation -> Expectation
subTest assumption test_code = 
    it assumption $ do 
        test_code

testDescr :: [Char] -> [Char] -> IO () -> IO()
testDescr function_name assumption test_code =
    hspec $ do
        describe function_name $ do
            it assumption $ do 
                test_code

