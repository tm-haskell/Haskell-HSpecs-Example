module OddEven where 

import Test.Hspec
import HW01 
import TestTemplate

allOddEvenTests = do 
    swapsEven
    swapsOdd
    ignoresDifferent
    combined

swapsEven = test "oddEven swaps adj even numbers"
    (oddEven [2,12] `shouldBe` [12,2])

swapsOdd = test "oddEven swaps adj odd numbers"
    (oddEven [3,13] `shouldBe` [13,3])

ignoresDifferent = test "oddEven ignores adj odd and even numbers"
    (oddEven [2,5,6] `shouldBe` [2,5,6])

combined = test "oddEven combined"
    (oddEven [1,13,2,5,2,12] `shouldBe` [13,1,2,5,12,2])