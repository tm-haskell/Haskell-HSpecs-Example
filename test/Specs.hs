module Main  where
import Test.Hspec
import Control.Exception (evaluate)
import HW01

import FactorialTest
import ListSum
import OddEven
import Power
import ExtPart
import Frequency

import TestTemplate


main :: IO ()
main = all_tests

--     it "returns the first element of an *arbitrary* list" $
--       property $ \x xs -> head (x:xs) == (x :: Int)

        -- it "throws an exception if used with an empty list" $ do
        --     evaluate (head []) `shouldThrow` anyException


-- HSpec test template

all_tests = do 
    -- factTests     
    -- allListsumTests
--     allOddEvenTests
    -- powerTests
    -- extPartTests
    frequencyTests


