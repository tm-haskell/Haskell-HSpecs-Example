module Frequency where 

import Test.Hspec
import Control.Exception
import HW01 
import TestTemplate
    
frequencyTests = hspec $ do 
    describe "Frequency" $ do
        subTest "Frequency only test"
            (frequency [1, 2, 1, 13, 45, 45] `shouldBe` [(1,2), (2,1), (13,1), (45,2) ])