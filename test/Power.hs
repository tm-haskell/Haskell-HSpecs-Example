module Power where 

import Test.Hspec
import Control.Exception (evaluate)
import HW01 
import TestTemplate

powerTests = hspec $ do 
    describe "Power" $ do

        subTest "power3 only test"
            (take 4 power3 `shouldBe` [1, 8, 27, 64])
        
        subTest "toPower3 only test"
            (take 4 toPower3 `shouldBe` [3, 9, 27, 81])

    describe "sumPower3" $ do
        subTest "sumPower3 only test"
            (sumPower3 4 `shouldBe` (3 + 9 + 27 + 81))

        subTest "sumPower positive input"
            (sumPower 2 5 `shouldBe` (2 + 4 + 8 + 16 + 32))
        
        subTest "sumPower negative base"
            (sumPower (-2) 3 `shouldBe` (-2 + 4 -8))

        subTest "sumPower is negative for negative base and odd length"
            (sumPower (-2) 103 `shouldSatisfy` (< 0))

        subTest "sumPower is positive for negative base and even length"
            (sumPower (-2) 154 `shouldSatisfy` (> 0))

        subTest "sumPower throws if negative length"
            (evaluate (sumPower 2 (-1)) `shouldThrow` anyException)
        