module Utils where 


takeI :: Integer -> [Integer] -> [Integer]
takeI n xs = take (fromInteger n) xs

areSameParity :: Int -> Int -> Bool  
areSameParity x y = even x == even y

threes = [3,3..]
nonNegatives = [0..]
naturals = [1..]

copyInfinite :: Integer -> [Integer]
copyInfinite x = [x,x..]

-- i is [0,...]
powersOf :: Integer -> [Integer]
powersOf base =  
    map (\(base, power) -> base ^ power) 
        (zip (copyInfinite base) nonNegatives)

-- sequence of m^i, m is fixed, i is all naturals [1,2..]
naturalPowersOf :: Integer -> [Integer]
naturalPowersOf base =  tail (powersOf base)

-- powers start from 0
firstNPowersOf :: Integer -> Integer -> [Integer]
firstNPowersOf base len = takeI len (powersOf base)

firstNNaturalPowersOf :: Integer -> Integer -> [Integer]
firstNNaturalPowersOf base len = tail (firstNPowersOf base (len + 1))