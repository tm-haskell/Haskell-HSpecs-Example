-- {-# OPTIONS_GHC -Wall #-}
module HW01 where
import Utils
import Debug.Trace
import Data.Map (fromListWith, toList)


factorial :: Integer -> Integer
factorial x 
    | x < 0 = error "x must be >= 0"
    | (x == 0 || x == 1) = 1
    | otherwise = product [1,2..x]


listSum :: [Int]-> [Int] -> [Int]
listSum = zipWith (+)


oddEven :: [Int] -> [Int]
oddEven [] = []
oddEven [x] = [x]
oddEven (fst:snd:rest) = 
    if areSameParity fst snd then 
        snd:oddEven (fst:rest) else 
            fst:oddEven (snd:rest)


-- how about calling this "cubes"
power3 :: [Integer]
power3 = map (\x -> x * x * x) [1,2..]


-- how about calling this "powersOf3"
toPower3 :: [Integer]
toPower3 = naturalPowersOf 3

sumPower3 :: Integer -> Integer
sumPower3 len = sumPower 3 len

-- natural powers
sumPower :: Integer -> Integer -> Integer
sumPower base len
    | len < 0 = error "len must be >="
    | otherwise = sum (firstNNaturalPowersOf base len)

expPart :: Integer -> Integer -> Double
expPart base len
    | len <= 0 = error "len must be > 0"
    | base < 0 = error "base must be >= 0"
    | otherwise =
        let powers = firstNPowersOf base len
            factorials = takeI len factorialsM in
                (sum 
                    (zipWith 
                        (\pow fact -> fromIntegral pow / fromIntegral fact) 
                            powers factorials)
                )

-- how about calling this "naturalFactorials" or "factorialSequence"?
-- first element is factorial of 1, not zero - [1, 2, 6...]
factorialsM :: [Integer]
factorialsM =  1 : zipWith (*) factorialsM [2..]

frequency :: [Int] -> [(Int, Int)]
frequency xs = toList (fromListWith (+) [(x, 1) | x <- xs])


